module DOM

using Gumbo: HTMLDocument, HTMLElement, HTMLNode, HTMLText, NullNode, children,
             prettyprint

const Document = HTMLDocument
const Element = HTMLElement
const Node = HTMLNode
const Text = HTMLText

Attribute((; first, second)::Pair{Symbol}) = string(first) => string(second)
Attribute(attr::Pair{String}) = attr
Attribute(name::Symbol) = string(name) => ""

Attributes() = Attributes(())
Attributes(; kwargs...) = Dict{String,String}([Attribute(kw) for kw in kwargs])
Attributes(args...) = Attributes(args)
Attributes(args::Tuple) = Dict{String,String}(Attribute.(args))

Base.show(io::IO, ::MIME"text/html", document::Union{Document,Node}) = prettyprint(io, document)

function BaseDocument()
    html = Element{:html}(
        Node[],
        NullNode(),
        Attributes(; dir = :ltr, lang = "en-US")
    )

    head = Element(:head)
    appendChild!(html, head)
    appendChild!(
        head,
        Element{:meta}(Node[], head, Attributes(; charset = "utf-8"))
    )
    appendChild!(
        head,
        Element{:meta}(
            Node[],
            head,
            Attributes(
                content = "width=device-width, initial-scale=1",
                name = :viewport
            )
        )
    )

    appendChild!(html, Element(:body))

    return Document("html", html)
end

function appendChild!(parent::Element, child::Node)
    # Orphan the child before assigning it to the new parent, but only if it's
    # actually being assigned to a new parent
    child.parent !== parent && removeChild!(child)

    # Early out if the child doesn't change parents
    child ∈ children(parent) && return child

    child.parent = parent
    push!(parent, child)

    return child
end
# Convenience method for the common scenario of quickly adding a `child` to the
# first child of the provided `Element` corresponding to the provided `tag`.
# For instance, adding a `child` to the `head` of an `html` tag given a
# `Document`.
function appendChild!(element::Element, target_tag::Symbol, child)
    target = first(getElementsByTagName(element, target_tag))
    return appendChild!(target, child)
end
function appendChild!((; root)::Document, target_tag::Symbol, child)
    return appendChild!(root, target_tag, child)
end
# Convenience function for children that may not look like HTML elements yet,
# but can be turned into them
function appendChild!(parent, children...)
    return appendChild!(parent, dom.(children))
end
function appendChild!(parent, children::Tuple)
    return appendChild!.((parent,), children)
end

function getElementsByTagName(element::Element, name::Symbol)
    return filter(Base.Fix2(isa, Element{name}), children(element))
end
getElementsByTagName((; root)::Document, name::Symbol) = getElementsByTagName(root, name)

dom(node::Node) = node
dom(text::AbstractString) = Text(text)
dom(value::Any) = error("$(typeof(value)) is not convertible into a `DOM.Element`")

function removeChild!(parent::Element, child::Node)
    filter!(parent_child -> child !== parent_child, children(parent))
    child.parent = NullNode()

    return child
end
removeChild!(::NullNode, child::Node) = child
removeChild!(child::Node) = removeChild!(child.parent, child)

end
